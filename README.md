# **WORK IN PROGRESS**

# OMOP-NOTE-DEID

The OMOP French* note deidentication Pipeline.

## Prerequire

- install java 8
- install maven (optionnal)
- install git (optionnal)
- python 3.5+

c

## Installation

__OMOP-NOTE-DEID__ is fully packaged and precompiled with the below projects:

- [TAG SERVE](https://github.com/strayMat/tag_serve)
- [Heideltime](https://framagit.org/interchu/heideltime)
- [UIMA APHP](https://framagit.org/interchu/uima-aphp)
- [SPARK OMOP DEID](https://framagit.org/interchu/omop-spark/tree/master/omop-spark-deid)
- [OpenNLP French Pos Tagger](https://enicolashernandez.blogspot.com/2012/12/apache-opennlp-fr-models.html)
- [Stanford Parser](https://github.com/stanfordnlp/CoreNLP)
- [java-string-similarity](https://github.com/tdebatty/java-string-similarity)
- [BRAT](https://github.com/nlplab/brat)
- [BRATEVAL](https://bitbucket.org/nicta_biomed/brateval/src)


- <Download and unarchive http://to_be_defined.com>_
- install the python dependency


## Compilation

It is also possible to compile the code yourself:


``` bash
# compile spark-etl
git clone git@framagit.org:parisni/spark-etl.git
cd spark-etl; mvn install; cd ..

# compile heideltime
git clone git@framagit.org:interchu/heideltime.git
cd heideltime; mvn install; cd ..

# compile uima-aphp
git clone git@framagit.org:interchu/uima-aphp.git
cd uima-aphp; mvn install; cd ..

# compile omop-spark
git clone git@framagit.org:interchu/omop-spark.git
cd omop-spark; mvn install; cd ..

# get neural
git clone ...
cd ... ; pip3 install -r requirement.txt
```

## Usage 

``` bash
# edit and configure the makefile
make init extract transform assign generate
```

## Language

- French: regular expressions, tokenizer, date normalizer are configured and evaluated with french notes.
- Other: both apache uima and the neuronal model are likely to adapt to other languages


## Process

This mode deidentify the notes that are not yet deidentified.
Also, it takes into account the previous candidate assigned for a given
patient.
Extra steps include : Subset, Merge, Export.

![pipeline](doc/overall.png)

Input Tables:

- note_subset (produced from note_nlp_deid and note)
- observation
- note_nlp_deid
- person_shift
- candidate
- location_history
- location

Output Merged Tables:

- note_nlp_deid
- note_deid

### Input Tables

The input tables is a subset of OMOP tables. Also, a subset of OMOP columns are
mandatory. The person_shift and candidate tables are external tables used to
replace by fictive information.

note:

- note_id: bigint
- person_id: bigint
- note_text: string

observation:

- person_id: bigint
- observation_type_concept_id: int
- observation_concept_id: int
- observation_source_value: string
- value_as_string: string

location:

- location_id: bigint
- address: string
- address_1: string
- address_2: string
- city: string
- zip: string

location_history:

- location_id: bigint
- domain_id: string
- entity_id: bigint

person_shift:

- person_id: bigint
- shift: int

candidate:

- type: string
- snippet: string

### Output tables

deid_note:
- note_id: bigint
- note_text: string
- create_datetime: timestamp

deid_note_nlp:
- note_id: bigint
- person_id: bigint
- offset_begin: int
- offset_end: int
- note_nlp_source_value: string
- snippet: string
- lexical_variant: string
- term_temporal: string
- nlp_system: string
- create_datetime: timestamp

## Input Formats

- [x] csv files
- [x] parquet/orc files
- [x] delta files
- [x] hive connector
- [ ] postgres connector (please ask)


## Performances

3 GPU / 200 spark executors with 5 cores and 1GO ram 

### Entity extraction + Fictive assignation:


- 1M notes per hour

### Deid Note Generation

- 30M notes in 15 minutes

